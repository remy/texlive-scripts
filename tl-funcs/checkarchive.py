#!/usr/bin/python

import sys
import hashlib
import zipfile

from tlpkg import TLPDB

def check_file(path):
    db = TLPDB()
    with zipfile.ZipFile(path, mode='r') as z:
        for member in z.infolist():
            if not member.filename.endswith(".tar.xz"):
                continue
            pkg = member.filename[:-len(".tar.xz")]
            info = db.get_info(pkg)
            if info is None:
                print("warning: unknown package %s" % pkg)
                continue
            data = z.read(member)
            dbsize = int(info["containersize"])
            digest = info["containerchecksum"]
            if len(data) != dbsize:
                print("%s: bad size %d, expecting %d" % (pkg,
                      len(data), dbsize))
            h = hashlib.sha512()
            h.update(data)
            if h.hexdigest() != digest:
                print("%s: bad SHA512 %s, expecting %s" % (pkg,
                      h.hexdigest(), digest))

def main():
    check_file(sys.argv[1])

if __name__ == "__main__":
    main()
