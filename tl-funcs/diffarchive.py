import argparse
import zipfile

def main():
    p = argparse.ArgumentParser()
    p.add_argument("zip1")
    p.add_argument("zip2")
    args = p.parse_args()

    l1 = read_pkglist(args.zip1)
    l2 = read_pkglist(args.zip2)
    diff_pkglists(l1, l2)

def read_pkglist(path):
    pkglist = []
    with zipfile.ZipFile(path, mode='r') as z:
        listf = z.open('CONTENTS')
        for l in listf:
            l = l.strip().decode('ascii')
            if l.isspace() or l.startswith('#'):
                continue
            pkglist.append(l.split())
    return pkglist

def diff_pkglists(oldpkglist, newpkglist):
    oldpkgdict = dict(oldpkglist)
    newpkgdict = dict(newpkglist)
    downloadlist = []
    deletelist = []
    # find new packages
    for entry in newpkglist:
        pkg = entry[0]
        newpkgver = entry[1]
        if pkg not in oldpkgdict:
            print("- new package", pkg)
            downloadlist.append(pkg)
        if pkg in oldpkgdict:
            if oldpkgdict[pkg] < newpkgver:
                print("- upgrade package", pkg, oldpkgdict[pkg], "->", newpkgver)
                downloadlist.append(pkg)
    # find deleted packages
    for entry in oldpkglist:
        if entry[0] not in newpkgdict:
            print("- deleted package", entry[0])
            deletelist.append(entry[0])
    return downloadlist, deletelist

if __name__ == "__main__":
    main()
