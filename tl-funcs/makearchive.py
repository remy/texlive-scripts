#!/usr/bin/python3

import sys
import os
import argparse
import re
import subprocess
import shutil
import zipfile
import requests

from tlconfig import CTANURL, PKGLISTDIR, YEAR

def find_latest_pkglist(pkgname):
    ver = 0
    pkglist = None
    for i in os.listdir(PKGLISTDIR):
        r = re.match(pkgname + "_([0-9]+).pkgs", i)
        if r:
            newlistfile = os.path.join(PKGLISTDIR, i)
            newver = int(r.group(1))
            if newver > ver:
                ver = newver
                with open(newlistfile) as f:
                    newls = f.readlines()
                    pkglist = [l.split() for l in newls]
    if pkglist is None:
        raise IOError("No package list found for %s" % pkgname)
    return ver, pkglist, newls

def write_contents(zfile, pkglist):
    # create CONTENTS file
    preamble = [
        "# These are the CTAN packages bundled in this package.\n",
        "# They were downloaded from " + CTANURL + "\n",
        "# The svn revision number (on the TeXLive repository)\n",
        "# on which each package is based is given in the 2nd column.\n"
    ]
    with open("CONTENTS", 'w') as f:
        for line in preamble:
            f.write(line)
        for pkg, ver in pkglist:
            f.write("%s %s\n" % (pkg, ver))

    # add CONTENTS file to the zipfile
    zfile.write("CONTENTS", compress_type=zipfile.ZIP_DEFLATED)
    print("    deflated CONTENTS")
    os.remove("CONTENTS")

def copy_maps(pkgname, mapfile):
    if os.path.exists(mapfile):
        shutil.copyfile(mapfile, pkgname+".maps")
        print("%s -> %s" % (mapfile, pkgname+".maps"))

def copy_fmts(pkgname, fmtfile):
    if os.path.exists(fmtfile):
        shutil.copyfile(fmtfile, pkgname+".fmts")
        print("%s -> %s" % (fmtfile, pkgname+".fmts"))

def update_pkgbuild(pkgver, shas):
    # modify PKGBUILD
    with open("PKGBUILD", 'r') as f:
        origlines = [l for l in f]
    with open("PKGBUILD", 'w') as w:
        lines = iter(origlines)
        for line in lines:
            if line.startswith("pkgver"):
                line = "pkgver=%s.%s\n" % (YEAR, pkgver)
                w.write(line)
                continue
            if line.startswith("sha256sum"):
                # output sha256s
                oldlines = [line]
                while ')' not in line:
                    line = next(lines)
                    oldlines.append(line)
                w.write("sha256sums=('{}'".format(shas[0]))
                for m in shas[1:]:
                    w.write("\n            '{}'".format(m))
                if len(oldlines) > len(shas):
                    w.write("\n")
                    for l in oldlines[len(shas):]:
                        w.write(l)
                else:
                    w.write(')\n')
                continue
            w.write(line)

def main():
    p = argparse.ArgumentParser()
    p.add_argument("--dry-run", dest="dryrun", action="store_true")
    p.add_argument("pkgname")
    args = p.parse_args()
    pkgname = args.pkgname

    # fail when more than one file with *.pkgs is found
    for i in os.listdir(pkgname):
        if i.startswith(pkgname + "_") and i.endswith(".pkgs"):
            print("WARNING: Already found a *.pkgs file for", pkgname + ". Skipping")
            sys.exit(0)

    print("Making new source archive for", pkgname)

    # look for the package list
    newver, pkglist, newls = find_latest_pkglist(pkgname)
    print("Package list is revision", newver)

    pkglistdir = os.path.abspath(PKGLISTDIR)

    os.chdir(pkgname)

    if args.dryrun:
        for pkg, version in pkglist:
            print("dry run: not fetching %s (revision %s)" % (pkg, version))
        return

    print("Downloading packages...")
    downloadlist = [name for (name, version) in pkglist]
    for pkg in downloadlist:
        if os.access(pkg + ".tar.xz", os.F_OK):
            os.unlink(pkg + ".tar.xz")
    s = requests.Session()
    failed = []
    for pkg in downloadlist:
        pkgurl = CTANURL + "/" + pkg + ".tar.xz"
        resp = s.get(pkgurl, stream=True)
        if not resp.ok:
            print("failed to download", pkgurl)
            failed.append(pkg)
            continue
        size = 0
        with open(pkg + ".tar.xz", "wb") as f:
            for chunk in resp.iter_content(chunk_size=None):
                f.write(chunk)
                size += len(chunk)
        print("  downloaded %s (%d bytes)" % (pkgurl, size))

    print(f"Failed to download {failed}")

    print("* Zipping sources together...")
    zipname = "%s-%s.%d-src.zip" % (pkgname, YEAR, newver)
    z = zipfile.ZipFile(zipname, mode='w', compression=zipfile.ZIP_STORED)
    for pkg, version in pkglist:
        z.write(pkg + ".tar.xz")
        print("    stored " + pkg + ".tar.xz")

    print("* Updating package list file")
    write_contents(z, pkglist)
    z.close()

    # Cleanup
    for pkg, _ in pkglist:
        if os.access(pkg + ".tar.xz", os.F_OK):
            os.unlink(pkg + ".tar.xz")

    mapfile = os.path.join(pkglistdir, pkgname+".maps")
    if os.path.exists(mapfile):
        print("* Copying new font map list")
        copy_maps(pkgname, mapfile)
    fmtfile = os.path.join(pkglistdir, pkgname+".fmts")
    if os.path.exists(fmtfile):
        print("* Copying new format directives")
        copy_fmts(pkgname, fmtfile)

    print("* Updating PKGBUILD")
    sources = [zipname]
    if os.path.exists(mapfile):
        sources.append(mapfile)
    if os.path.exists(fmtfile):
        sources.append(fmtfile)
    shas = []
    for s in sources:
        shazip = subprocess.check_output(["sha256sum", s], universal_newlines=True)
        shazip = shazip.split()[0]
        shas.append(shazip)
    update_pkgbuild(newver, shas)
    for (s, sha) in zip(sources, shas):
        print(s, sha)
    print("pkgver=%s" % newver)

if __name__ == "__main__":
    main()

# vim: set ts=4 et tw=0:
