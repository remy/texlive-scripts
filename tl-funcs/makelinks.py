#!/usr/bin/python

import glob
import os
import sys

from tlpkg import TLPDB

def main():
    pkgs = [
        "bibtexextra",
        "core",
        "fontsextra",
        "formatsextra",
        "games",
        "humanities",
        "langchinese",
        "langcyrillic",
        "langextra",
        "langgreek",
        "langjapanese",
        "langkorean",
        "latexextra",
        "music",
        "pictures",
        "pstricks",
        "publishers",
        "science"
    ]
    for p in pkgs:
        print("===", "texlive-" + p, "===")
        show_links("texlive-"+p)
        print("")

def show_links(archpkg):
    db = TLPDB()
    plist = glob.glob("pkglists/"+archpkg+"_*.pkgs")[0]
    with open(plist) as f:
        for line in f:
            pkg, _ = line.split()
            info = db.get_info(pkg)
            if info is None:
                print("warning: unknown package %s" % pkg)
                continue

            files = info.get("runfiles", [])

            if (pkg+".ARCH") in info.get("depend", []):
                binpkg = db.get_info(pkg+".x86_64-linux")
                binfiles = binpkg.get("binfiles", []) if binpkg else []
                for item in binfiles:
                    if item.startswith("arch="):
                        continue
                    filename = item.split()[0]
                    base = os.path.basename(filename)
                    basetargets = ["/"+base+ext
                                   for ext in ("", ".sh", ".py", ".pl", ".rb", ".lua")]
                    targets = [f for f in files if f.endswith(tuple(basetargets))]
                    print(pkg, base, ' '.join(targets))

if __name__ == "__main__":
    main()
