"""
Generate package lists, font map and format configs
"""

import io
import os

from tlpkg import TLPDB

ARCH_PACKAGES = {
    "texlive-core": [
        "collection-basic",
        "collection-binextra",
        "collection-context",
        "collection-fontsrecommended",
        "collection-fontutils",
        "collection-langcjk",
        "collection-langczechslovak",
        "collection-langenglish",
        "collection-langeuropean",
        "collection-langfrench",
        "collection-langgerman",
        "collection-langitalian",
        "collection-langpolish",
        "collection-langportuguese",
        "collection-langspanish",
        "collection-latex",
        "collection-latexrecommended",
        "collection-luatex",
        "collection-metapost",
        "collection-plaingeneric",
        "collection-xetex",
        "bidi",
        "iftex",
        "pgf",
        "ruhyphen",
        "ukrhyph",
        "hyphen-*",
        # exclude tlcockpit (65MB), Scala GUI for tlmgr
        "-tlcockpit",
    ],
    "texlive-bibtexextra": ["collection-bibtexextra"],
    "texlive-fontsextra": [
        "collection-fontsextra",
        # exclude omega packages
        "-ocherokee",
        "-oinuit",
    ],
    "texlive-formatsextra": [
        "collection-formatsextra",
        # exclude aleph
        "-aleph",
    ],
    "texlive-games": ["collection-games"],
    "texlive-humanities": ["collection-humanities"],
    "texlive-langchinese": ["collection-langchinese", "-hyphen-*"],
    "texlive-langcyrillic": [
        "collection-langcyrillic",
        "-ruhyphen",
        "-ukrhyph",
        "-hyphen-*",
    ],
    "texlive-langextra": [
        "collection-langarabic",
        "collection-langother",
        "-bidi",
        "-ebong",
        "-hyphen-*",
    ],
    "texlive-langgreek": ["collection-langgreek", "-hyphen-*"],
    "texlive-langjapanese": ["collection-langjapanese"],
    "texlive-langkorean": ["collection-langkorean"],
    "texlive-latexextra": ["collection-latexextra"],
    "texlive-music": ["collection-music"],
    "texlive-pictures": ["collection-pictures", "-pgf"],
    "texlive-pstricks": ["collection-pstricks"],
    "texlive-publishers": ["collection-publishers"],
    "texlive-science": ["collection-mathscience"],
}

def genpkglist(archpkg, outdir):
    db = TLPDB()
    # Build package collection
    pkgs = set()
    def add_pkg(p):
        """
        add_pkg adds `p` to pkgs. If `p` is a collection,
        dependencies are added but *not* recursively
        """
        pkgs.add(p)
        info = db.get_info(p)
        if info is None:
            raise IndexError("no such package " + p)
        if p.startswith("collection-"):
            for dep in info["depend"]:
                pkgs.add(dep)

    for p in ARCH_PACKAGES[archpkg]:
        if p.startswith("-"):
            if p.endswith("*"):
                for _p in list(pkgs):
                    if _p.startswith(p[1:-1]):
                        pkgs.remove(_p)
            else:
                pkgs.remove(p[1:])
        elif p.endswith("*"):
            # simple glob
            for _p in db.packages():
                if _p.startswith(p[:-1]):
                    add_pkg(_p)
        else:
            add_pkg(p)

    def write_file(basename, sio):
        if sio.tell():
            with open(os.path.join(outdir, basename), "w") as w:
                w.write(buf.getvalue())

    # generate package list
    buf = io.StringIO()
    rev = 0
    for p in sorted(pkgs):
        info = db.get_info(p)
        # not all packages should be output
        if p.startswith("collection-"):
            continue
        if archpkg == "texlive-core" and not has_runfiles(info):
            continue
        # remember highest revision
        if int(info["revision"]) > rev:
            rev = int(info["revision"])
        print(p, info["revision"], file=buf)
    write_file("%s_%d.pkgs" % (archpkg, rev), buf)
    # Formats
    formats = []
    for p in sorted(pkgs):
        info = db.get_info(p)
        for cmd in sorted(info.get("execute", [])):
            if cmd.startswith("AddFormat"):
                formats.append(cmd)
    buf = io.StringIO()
    for cmd in sorted(formats):
        print(convert_addformat(cmd), file=buf)
    write_file(archpkg + ".fmts", buf)
    # Maps
    maps = []
    for p in sorted(pkgs):
        info = db.get_info(p)
        for cmd in info.get("execute", []):
            if cmd.startswith(("addMap", "addKanjiMap", "addMixedMap")):
                maps.append(cmd[len("add"):])
    maps.sort()
    if maps:
        with open(os.path.join(outdir, archpkg+".maps"), "w") as w:
            for m in maps:
                print(m, file=w)

def has_runfiles(info):
    for f in info.get("runfiles", []):
        if "RELOC" in f or "texmf-dist" in f:
            return True
    return False

def convert_addformat(line):
    """
    >>> convert_addformat('AddFormat name=tex engine=tex options="tex.ini" '
    ... '  fmttriggers=cm,hyphen-base,knuth-lib,plain')
    'tex tex - tex.ini'
    >>> convert_addformat('AddFormat name=cont-en engine=xetex    '
    ... '     patterns=cont-usr.tex options="-8bit *cont-en.mkii"')
    'cont-en xetex cont-usr.tex -8bit *cont-en.mkii'
    >>> convert_addformat('AddFormat name=cont-fr mode=disabled engine=pdftex  '
    ... '   patterns=cont-usr.tex options="-8bit *cont-fr.mkii" fmttriggers=context')
    '#! cont-fr pdftex cont-usr.tex -8bit *cont-fr.mkii'
    """
    # parse line
    values = {"patterns": "-", "options": ""}
    if line.startswith('AddFormat '):
        line = line[len('AddFormat '):]
    while line:
        field, _, rest = line.partition("=")
        if rest.startswith('"'):
            value, _, rest = rest[1:].partition('"')
        else:
            value, _, rest = rest.partition(" ")
        values[field] = value
        line = rest.strip()
    prefix = "#! " if values.get("mode") == "disabled" else ""
    return prefix + "{name} {engine} {patterns} {options}".format(**values)

def main():
    import argparse
    p = argparse.ArgumentParser()
    p.add_argument("outdir", metavar="DIRECTORY", help="output directory")
    p.add_argument("PACKAGE", nargs='*')
    args = p.parse_args()

    if not args.PACKAGE:
        args.PACKAGE = sorted(ARCH_PACKAGES)
    for p in args.PACKAGE:
        genpkglist(p, args.outdir)

if __name__ == "__main__":
    main()
