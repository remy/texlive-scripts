#!/usr/bin/python

import glob
import os
import sys

from tlpkg import TLPDB


def main():
    pkgs = [
        "bibtexextra",
        "core",
        "fontsextra",
        "formatsextra",
        "games",
        "humanities",
        "langchinese",
        "langcyrillic",
        "langextra",
        "langgreek",
        "langjapanese",
        "langkorean",
        "latexextra",
        "music",
        "pictures",
        "pstricks",
        "publishers",
        "science",
    ]
    for p in pkgs:
        print("===", "texlive-" + p, "===")
        show_manpages("texlive-" + p)
        print("")


def show_manpages(archpkg):
    db = TLPDB()
    plist = glob.glob("pkglists/" + archpkg + "_*.pkgs")[0]
    with open(plist) as f:
        for line in f:
            pkg, _ = line.split()
            info = db.get_info(pkg)
            if info is None:
                print("warning: unknown package %s" % pkg)
                continue

            files = info.get("docfiles", [])
            for f in files:
                if "/man/" in f and f.endswith((".1", ".5")):
                    print(pkg, f)


if __name__ == "__main__":
    main()
