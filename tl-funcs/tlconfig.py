YEAR = 2023

#CTANURL="http://mirror.ctan.org/systems/texlive/tlnet/archive/"
#CTANURL="https://ctan.gutenberg.eu.org/systems/texlive/tlnet/archive/"
# Explicit mirror
CTANURL="http://ftp.tu-chemnitz.de/pub/tex/systems/texlive/tlnet/archive/"
# Pretest URL
#CTANURL="https://texlive.info/tlpretest/archive/"

PKGLISTDIR = "pkglists"

