class TLPDB(object):
    def __init__(self, dbfile="tlpkg/texlive.tlpdb"):
        self._info = {}
        with open(dbfile) as f:
            self._parse(f)

    lists = {
        "depend",
        "logdesc",
        "execute",
    }

    multi = {
        "binfiles",
        "docfiles",
        "runfiles",
        "srcfiles",
    }

    def packages(self):
        return iter(self._info)

    def get_info(self, pkg):
        return self._info.get(pkg)

    def _parse(self, f):
        pkg = None
        field = None
        for line in f:
            if line.isspace():
                pkg = None
                field = None
                continue
            if line.startswith(" "):
                value = line.strip()
                self._info[pkg][field].append(value)
            else:
                field, _, value = line.strip().partition(" ")
                if field == "name":
                    pkg = value
                    self._info[pkg] = {}
                elif field in self.multi or field in self.lists:
                    self._info[pkg].setdefault(field, [])
                    self._info[pkg][field].append(value)
                else:
                    self._info[pkg][field] = value


