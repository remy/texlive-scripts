#!/usr/bin/python3

import sys
import os
import argparse
import re
import subprocess
import shutil
import zipfile
import requests

from tlconfig import CTANURL, PKGLISTDIR, YEAR

def find_latest_zip(pkgname, newver):
    "Finds latest zip file before newver"
    year = 0
    ver = 0
    for i in os.listdir(pkgname):
        r = re.match("%s-([0-9]{4})\\.([0-9]+)-src.zip" % pkgname, i)
        if r:
            this_year = int(r.group(1))
            this_ver = int(r.group(2))
            if this_ver > ver and this_ver <= newver:
                year = this_year
                ver = this_ver
                pkglist = []
                with zipfile.ZipFile(os.path.join(pkgname, i), mode='r') as z:
                    listf = z.open('CONTENTS')
                    for l in listf:
                        l = l.strip().decode('ascii')
                        if l.isspace() or l.startswith('#'):
                            continue
                        pkglist.append(l.split())
    if year == 0:
        raise IOError("No previous zip package found for %s" % pkgname)
    return year, ver, pkglist

def find_latest_pkglist(pkgname):
    ver = 0
    pkglist = None
    for i in os.listdir(PKGLISTDIR):
        r = re.match(pkgname + "_([0-9]+).pkgs", i)
        if r:
            newlistfile = os.path.join(PKGLISTDIR, i)
            newver = int(r.group(1))
            if newver > ver:
                ver = newver
                with open(newlistfile) as f:
                    pkglist = set()
                    for l in f:
                        pkgname, pkgver = l.split()
                        pkglist.add((pkgname, pkgver))
    if pkglist is None:
        raise IOError("No package list found for %s" % pkgname)
    return ver, sorted(pkglist)

def diff_pkglists(oldpkglist, newpkglist, log=None):
    oldpkgdict = dict(oldpkglist)
    newpkgdict = dict(newpkglist)
    downloadlist = []
    deletelist = []
    # find new packages
    for entry in newpkglist:
        pkg = entry[0]
        newpkgver = entry[1]
        if pkg not in oldpkgdict:
            print("- new package", pkg)
            if log:
                print("- new package", pkg, file=log)
            downloadlist.append(pkg)
        if pkg in oldpkgdict:
            if oldpkgdict[pkg] < newpkgver:
                print("- upgrade package", pkg, oldpkgdict[pkg], "->", newpkgver)
                if log:
                    print("- upgrade package", pkg, oldpkgdict[pkg], "->", newpkgver, file=log)
                downloadlist.append(pkg)
    # find deleted packages
    for entry in oldpkglist:
        if entry[0] not in newpkgdict:
            print("- deleted package", entry[0])
            if log:
                print("- deleted package", entry[0], file=log)
            deletelist.append(entry[0])
    return downloadlist, deletelist

def write_contents(zfile, pkglist):
    # create CONTENTS file
    preamble = [
        "# These are the CTAN packages bundled in this package.\n",
        "# They were downloaded from " + CTANURL + "\n",
        "# The svn revision number (on the TeXLive repository)\n",
        "# on which each package is based is given in the 2nd column.\n"
    ]
    with open("CONTENTS", 'w') as f:
        for line in preamble:
            f.write(line)
        for pkg, ver in pkglist:
            f.write("%s %s\n" % (pkg, ver))

    # add CONTENTS file to the zipfile
    zfile.write("CONTENTS", compress_type=zipfile.ZIP_DEFLATED)
    print("    deflated CONTENTS")
    os.remove("CONTENTS")

def copy_maps(pkgname, mapfile):
    if os.path.exists(mapfile):
        shutil.copyfile(mapfile, pkgname+".maps")
        print("%s -> %s" % (mapfile, pkgname+".maps"))

def copy_fmts(pkgname, fmtfile):
    if os.path.exists(fmtfile):
        shutil.copyfile(fmtfile, pkgname+".fmts")
        print("%s -> %s" % (fmtfile, pkgname+".fmts"))

def update_pkgbuild(pkgver, shas):
    # modify PKGBUILD
    with open("PKGBUILD", 'r') as f:
        origlines = [l for l in f]
    with open("PKGBUILD", 'w') as w:
        lines = iter(origlines)
        for line in lines:
            if line.startswith("pkgver"):
                line = "pkgver=%s.%s\n" % (YEAR, pkgver)
                w.write(line)
                continue
            if line.startswith("sha256sum"):
                # output sha256s
                oldlines = [line]
                while ')' not in line:
                    line = next(lines)
                    oldlines.append(line)
                w.write("sha256sums=('{}'".format(shas[0]))
                for m in shas[1:]:
                    w.write("\n            '{}'".format(m))
                if len(oldlines) > len(shas):
                    w.write("\n")
                    for l in oldlines[len(shas):]:
                        w.write(l)
                else:
                    w.write(')\n')
                continue
            w.write(line)

def main():
    p = argparse.ArgumentParser()
    p.add_argument("--dry-run", dest="dryrun", action="store_true")
    p.add_argument("pkgname")
    args = p.parse_args()
    pkgname = args.pkgname

    # look for new package list
    newver, newpkglist = find_latest_pkglist(pkgname)

    # read old package list
    try:
        oldyear, oldver, oldpkglist = find_latest_zip(pkgname, newver)
    except IOError as e:
        print(e)
        oldyear, oldver, oldpkglist = None, None, []

    print("Making new source archive for", pkgname)

    #if newver == oldver:
    #    print("Package", pkgname, "unchanged")
    #    sys.exit(0)

    print("Upgrading package", pkgname, "from", oldver, "to", newver)

    with open("changes." + pkgname, "w") as log:
        downloadlist, deletelist = diff_pkglists(oldpkglist, newpkglist, log=log)

    if args.dryrun:
        sys.exit(0)

    pkglistdir = os.path.abspath(PKGLISTDIR)

    os.chdir(pkgname)

    print("Extracting unchanged packages...")
    oldzipname = "%s-%s.%s-src.zip" % (pkgname, oldyear, oldver)
    z = zipfile.ZipFile(oldzipname, mode='r')
    z.extractall()
    z.close()

    print("Downloading packages...")
    for pkg in downloadlist:
        if os.access(pkg + ".tar.xz", os.F_OK):
            os.unlink(pkg + ".tar.xz")
    s = requests.Session()
    for pkg in downloadlist:
        pkgurl = CTANURL + "/" + pkg + ".tar.xz"
        resp = s.get(pkgurl, stream=True)
        if not resp.ok:
            print("failed to download", pkgurl)
            continue
        size = 0
        with open(pkg + ".tar.xz", "wb") as f:
            for chunk in resp.iter_content(chunk_size=None):
                f.write(chunk)
                size += len(chunk)
        print("  downloaded %s (%d bytes)" % (pkgurl, size))

    print("* Zipping sources together...")
    newzipname = "%s-%s.%d-src.zip" % (pkgname, YEAR, newver)
    z = zipfile.ZipFile(newzipname, mode='w', compression=zipfile.ZIP_STORED)
    for pkg, version in newpkglist:
        z.write(pkg + ".tar.xz")
        print("    stored " + pkg + ".tar.xz")

    print("* Updating package list file")
    write_contents(z, newpkglist)
    z.close()

    # Cleanup
    for p in set(name for (name, ver) in oldpkglist) | set(name for (name, ver) in newpkglist):
        if os.access(p + ".tar.xz", os.F_OK):
            os.unlink(p + ".tar.xz")

    mapfile = os.path.join(pkglistdir, pkgname+".maps")
    if os.path.exists(mapfile):
        print("* Copying new font map list")
        copy_maps(pkgname, mapfile)
    fmtfile = os.path.join(pkglistdir, pkgname+".fmts")
    if os.path.exists(fmtfile):
        print("* Copying new format directives")
        copy_fmts(pkgname, fmtfile)

    print("* Updating PKGBUILD")
    sources = [newzipname]
    if os.path.exists(mapfile):
        sources.append(mapfile)
    if os.path.exists(fmtfile):
        sources.append(fmtfile)
    shas = []
    for s in sources:
        shazip = subprocess.check_output(["sha256sum", s], universal_newlines=True)
        shazip = shazip.split()[0]
        shas.append(shazip)
    update_pkgbuild(newver, shas)
    for (s, sha) in zip(sources, shas):
        print(s, sha)
    print("pkgver=%s" % newver)


if __name__ == "__main__":
    main()

# vim: set ts=4 et tw=0:
